# zh_CN coa 2016-2020

#### 介绍
ERPNext 中国会计科目表.
逐渐完善中.
现在大项已经完成.

#### 软件架构
json 文件, 贴上即可用.

#### 样例
![输入图片说明](https://images.gitee.com/uploads/images/2020/1123/164535_bfca1825_7433720.png "2020-11-23_164246.png")


#### 安装教程

将json文件贴到
/where_is_your_erpnext/apps/erpnext/erpnext/accounts/doctype/account/chart_of_accounts/verified

#### 使用说明

在创建公司时选择标准模板,科目表选择"CN-2016-2020会计准则科目表"
![输入图片说明](https://images.gitee.com/uploads/images/2020/1123/161307_86a5c5f4_7433720.png "微信图片_20201123161253.png")

#### 参与贡献

Inspired By https://github.com/r3f/china-chart-of-accounts

#### 参考标准 
2020 中国会计师准则

http://www.canet.com.cn/kemu/596034.html

